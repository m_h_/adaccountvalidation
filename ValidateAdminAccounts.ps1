﻿<#
.SYNOPSIS
  Component of REDACTED
  Script for validation Admin Accounts
 
.DESCRIPTION
  Verify that all admin accounts have a z suffix
  Verify all admin accounts are in the admin OU. Remediate any exceptions.
  Disable all admin accounts that have not logged on in 3 months
  Flag admin account descriptions not in format "Firstname Lastname Admin Account"
  Output results to CSV files
 
.INPUTS
  <None>
 
.OUTPUTS
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\adminAccountsWithoutZ.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\oldAdminAccounts.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\unusedAdminAccounts.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\adminsWithBadDescription.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\lostAdmins.csv
   
.NOTES
  Version:        1.0
  Author:         Matt Hanson
  Creation Date:  22/03/2019
  Purpose/Change: Initial Scripting
 
.EXAMPLE
  
  ValidateAdminAccounts
#>

Function CheckAdminEndsWithZ {
    ####################################################################
    # Get accounts without "z" as last character of staffcode
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU
    )
    
    $adminAccountsWithoutZ = Get-ADUser -Filter {SamAccountName -notlike '*z'} -SearchBase $AdminsOU

    # Output results
    $adminAccountsWithoutZ | Export-Csv -Path "$($Path)\adminAccountsWithoutZ.csv"
    
    Write-Host "See '$($Path)\adminAccountsWithoutZ.csv' for admins without a staffcode ending in 'z'" `
        -ForegroundColor Yellow
}

Function GetInactiveAdmins {
    ####################################################################
    # Get Admins that haven't logged in for 3 months or longer
    # Disable them
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU,
        [string] $DisabledUsersOU
    )
    $currentDateTime = Get-Date

    $inactiveAdminAccounts = Get-ADUser -Properties LastLogonDate -SearchBase $AdminsOU `
        -Filter * | Where-Object LastLogonDate -LT ($currentDateTime).AddMonths(-3) `
        | Where-Object LastLogonDate -ne $null
    
    # Disable accounts
    ForEach($inactiveAdmin in $inactiveAdminAccounts) {
        Disable-ADAccount -Identity $inactiveAdmin.SamAccountName -WhatIf
    }
        
    # Move to Disabled OU
    ForEach($user in $inactiveAdminAccounts) {
        $GUID = (Get-ADUser $user).ObjectGUID
        Move-ADObject -Identity $GUID -TargetPath $DisabledUsersOU -WhatIf
    }

    # Output results
    $inactiveAdminAccounts | Export-Csv -Path "$($Path)\inactiveAdminAccounts.csv"

    Write-Host "Admins that have not logged in for over 3 months have been disabled." `
        -ForegroundColor Yellow
    Write-Host "See '$($Path)\inactiveAdminAccounts.csv' for Admins that have been disabled." `
        -ForegroundColor Yellow
}

Function GetUnusedAdmins {
    ####################################################################
    # Get contractors that have no LastLogonDate value
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU,
        [string] $DisabledUsersOU
    )
    $unusedAdminAccounts = Get-ADUser -Properties LastLogonDate `
        -Filter {Enabled -eq $true} -SearchBase $AdminsOU `
        | Where-Object LastLogonDate -eq $null
        
    # Disable accounts
    ForEach($unusedAdmin in $unusedAdminAccounts) {
        Disable-ADAccount -Identity $unusedAdmin.SamAccountName -WhatIf
    }
    
    # Move to Disabled OU
    ForEach($user in $unusedAdminAccounts) {
        $GUID = (Get-ADUser $user).ObjectGUID
        Move-ADObject -Identity $GUID -TargetPath $DisabledUsersOU -WhatIf
    }

    # Output results to basic csv
    $unusedAdminAccounts | Export-Csv -Path "$($Path)\unusedAdminAccounts.csv"

    Write-Host "See '$($Path)\unusedAdminAccounts.csv' for Admins that have never logged on" `
        -ForegroundColor Yellow
}

Function CheckAdminDescription {
    ####################################################################
    # Check admin description as "Firstname Lastname Admin Account"
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU
    )
    $adminAccounts = Get-ADUser -Properties Description -Filter * -SearchBase $AdminsOU

    # Check description of account
    $adminsWithBadDescription = @()

    ForEach($admin in $adminAccounts) {
        $fn = $admin.firstname
        $ln = $admin.lastname

        If($admin.Description -ne "$fn $ln Admin Account"){
            $adminsWithBadDescription += $admin
        }
    }

    # Output results
    $adminsWithBadDescription | Export-Csv -Path "$($Path)\adminsWithBadDescription.csv"

    Write-Host "See '$($Path)\unusedAdminAccounts.csv' for Admins with description not in correct format" `
        -ForegroundColor Yellow
}

Function FindLostAdmins {
    ####################################################################
    # Finds accounts with *admin* in its attributes not in correct admin OU
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $RootOU
    )

    # Search criteria: 
    #     account not in DisabledUsers OU
    #     description contains 'admin'
    #     SamAccountName ends with 'z'
    # Does this get all admins?
    $lostAdmins = Get-ADUser -Properties CanonicalName `
        -Filter { Description -like "*admin*" -and SamAccountName -like "*z" } -SearchBase $RootOU `
        | Where-Object { $_.CanonicalName -notlike "aar.com.au/Accounts/Administrators*" `
        -and $_.CanonicalName -notlike "aar.com.au/Disabled Accounts*" }
    
    # Output results
    $lostAdmins | Export-Csv -Path "$($Path)\lostAdmins.csv"

    Write-Host "See '$($Path)\lostAdmins.csv' for potential Admins that are not in the correct OU" `
        -ForegroundColor Yellow
}

Function ValidateAdminAccounts {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU,
        [string] $DisabledUsersOU,
        [string] $RootOU
    )
    
    CheckAdminEndsWithZ -Path $Path -AdminsOU $AdminsOU
    GetInactiveAdmins -Path $Path -AdminsOU $AdminsOU -DisabledUsersOU $DisabledUsersOU
    GetUnusedAdmins -Path $Path -AdminsOU $AdminsOU
    CheckAdminDescription -Path $Path -AdminsOU $AdminsOU
    FindLostAdmins -Path $Path -RootOU $RootOU
}
 
