﻿<#
.SYNOPSIS
  Component of REDACTED
  Script for validating mailboxes
 
.DESCRIPTION
  Verify all mailbox accounts are in the Mailbox OU. Remediate any exceptions.
  Verify that all mailboxes have a MBX prefix in UserPrincipalName and SamAccountName
  Verify all mailboxes have a description with owner 
  ? Disable all mailbox accounts are in the Mailbox OU. 
  Output results to CSV files
 
.INPUTS
  <None>
 
.OUTPUTS
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\####.csv

.NOTES
  Version:        1.0
  Author:         Matt Hanson
  Creation Date:  29/03/2019
  Purpose/Change: Initial Scripting
 
.EXAMPLE
  
  ValidateMailboxes
#>

Function GetLostMailboxes {
    ####################################################################
    # Get potential Mailboxes not in Mailboxes OU. Mailboxes identified by having mbx or mailbox in name, description, or UPN
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $RootOU
    )
    
    $filter = "(SamAccountName -like '*MBX*') -Or (SamAccountName -like '*MAILBOX*') " + `
        " -Or (Description -like '*MBX*') -Or (Description -like '*MAILBOX*')"

    $IOnceWasLost = Get-ADUser -Filter $filter -SearchBase $RootOU

    $ButNowImFound = @()
    ForEach($mailbox in $IOnceWasLost) {
        If($mailbox.DistinguishedName -notlike "*Mailboxes*"){
            $ButNowImFound += $mailbox
        }
    }

    # Output results
    $ButNowImFound | Export-Csv -Path "$($Path)\LostMailboxes.csv"
    
    Write-Host "See '$($Path)\LostMailboxes.csv' for potential Mailboxes not in Mailboxes OU." `
        -ForegroundColor Yellow
}

Function GetBadlyNamedMailboxes {
    ####################################################################
    # Get Mailboxes without 'MBX' at start of UserPrincipalName or SamAccountName
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $MailboxesOU
    )

    $BadSAMMailboxes = Get-ADUser -Filter { SamAccountName -notlike 'MBX*' } -SearchBase $MailboxesOU
   
    $BadUPNMailboxes = Get-ADUser -Filter * -SearchBase $SearchOU -Properties userPrincipalName `
        | Where-Object { $_.UserPrincipalName -notlike 'MBX*' }


    # Output results
    $BadSAMMailboxes | Export-Csv -Path "$($Path)\BadSAMMailboxes.csv"
    $BadUPNMailboxes | Export-Csv -Path "$($Path)\BadUPNMailboxes.csv"
    
    Write-Host "See '$($Path)\BadSAMMailboxes.csv' for Mailboxes without 'MBX' at start of SamAccountName." `
        -ForegroundColor Yellow
    Write-Host "See '$($Path)\BadUPNMailboxes.csv' for Mailboxes without 'MBX' at start of UserPrincipalName." `
        -ForegroundColor Yellow
}

Function GetMailBoxesWithoutDescription {
    ####################################################################
    # Verify all mailboxes have a description with owner
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $MailboxesOU
    )

    $MailboxesWithoutDescription = Get-ADUser -Properties Description -Filter * -SearchBase $MailboxesOU `
        | Where-Object { $_.Description -like "" } 

    # Output results
    $MailboxesWithoutDescription | Export-Csv -Path "$($Path)\MailboxesWithoutDescription.csv"
    
    Write-Host "See '$($Path)\MailboxesWithoutDescription.csv' for Mailboxes without a description." `
        -ForegroundColor Yellow
}

Function ValidateMailboxes {
   [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $MailboxesOU,
        [string] $RootOU
    )

    GetLostMailboxes -Path $Path -RootOU $RootOU
    GetBadlyNamedMailboxes -Path $Path -MailboxesOU $MailboxesOU
    GetMailBoxesWithoutDescription -Path $Path -MailboxesOU $MailboxesOU
}
 
