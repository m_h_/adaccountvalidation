﻿<#
.SYNOPSIS
  Component of REDACTED
  Script for validation the Contractors OU (aar.com.au/Accounts/Contractors)
 
.DESCRIPTION
  Identifies contractors in the Contractors OU that have not logged in within 3 months and disables them
  Identifies disabled accounts in Contractors OU and moves to Disabled Users OU
  Identifies any users in Contractors OU without an x on the end of staff code
  Output results to CSV files
 
.INPUTS
  <None>
 
.OUTPUTS
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\disabledContractors.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\contractorsWithoutX.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\unusedContractorAccounts.csv
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\contractorsWithoutX.csv
  
.NOTES
  Version:        1.0
  Author:         Matt Hanson
  Creation Date:  22/03/2019
  Purpose/Change: Initial Scripting
 
.EXAMPLE
  
  ValidateContractorsOU
#>

Function GetInactiveContractors {
    ####################################################################
    # Get contractors that haven't logged in for 3 months or longer
    # Disable them
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $ContractorsOU
    )

    $currentDateTime = Get-Date
    $inactiveContractorAccounts = Get-ADUser -Properties LastLogonDate `
        -Filter {Enabled -eq $true} -SearchBase $ContractorsOU `
        | Where-Object LastLogonDate -LT ($currentDateTime).AddMonths(-3) `
        | Where-Object LastLogonDate -ne $null
    
    # Disable accounts
    ForEach($inactiveContractor in $inactiveContractorAccounts) {
        Disable-ADAccount -Identity $inactiveContractor.SamAccountName -WhatIf
    }
    
    # Output results
    $inactiveContractorAccounts | Export-Csv -Path "$($Path)\inactiveContractorAccounts.csv"

    Write-Host "Contractors that have not logged in for over 3 months have been disabled." `
        -ForegroundColor Yellow
    Write-Host "See '$($Path)\inactiveContractorAccounts.csv' for contractors that have been disabled." `
        -ForegroundColor Yellow
}

Function GetUnusedContractors {
    ####################################################################
    # Get contractors that have no LastLogonDate value
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $ContractorsOU
    )

    $unusedContractorAccounts = Get-ADUser -Properties LastLogonDate `
        -Filter {Enabled -eq $true} -SearchBase $ContractorsOU `
        | Where-Object LastLogonDate -eq $null
        
    # Output results to basic csv
    $unusedContractorAccounts | Export-Csv -Path "$($Path)\unusedContractorAccounts.csv"

    Write-Host "See '$($Path)\unusedContractorAccounts.csv' for contractors that have never logged on" `
        -ForegroundColor Yellow
}

Function GetDisabledContractors {
    ####################################################################
    # Get disabled contractor accounts
    # Move to correct OU
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $ContractorsOU,
        [string] $DisabledUsersOU
    )

    Sleep -Seconds 5

    $disabledContractors = Get-ADUser -Filter { Enabled -eq $false } `
        -SearchBase $ContractorsOU
       
    # Move to Disabled OU
    ForEach($contractor in $disabledContractors) {
        $GUID = (Get-ADUser $contractor).ObjectGUID
        Move-ADObject -Identity $GUID -TargetPath $DisabledUsersOU -WhatIf
    }
   
    # Output results
    $disabledContractors | Export-Csv -Path "$($Path)\disabledContractors.csv"

    Write-Host "Disabled contractors moved to aar.com.au/Disabled Accounts/users." `
        -ForegroundColor Yellow
    Write-Host "See '$($Path)\disabledContractors.csv' for affected accounts" `
        -ForegroundColor Yellow
}

Function GetContractorsWithoutX {
    ####################################################################
    # Get accounts without "x" as last character of staffcode
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $ContractorsOU
    )

    $contractorsWithoutX = Get-ADUser -Filter { SamAccountName -notlike '*x' } `
        -SearchBase $ContractorsOU
    
    # Output results
    $contractorsWithoutX | Export-Csv -Path "$($Path)\contractorsWithoutX.csv"

    Write-Host "See '$($Path)\contractorsWithoutX.csv' for contractors without a staffcode ending in 'x'" `
        -ForegroundColor Yellow
}

Function ValidateContractorsOU {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $ContractorsOU,
        [string] $DisabledUsersOU
    )

    GetInactiveContractors -Path $Path -ContractorsOU $ContractorsOU
    GetUnusedContractors -Path $Path -ContractorsOU $ContractorsOU
    GetDisabledContractors -Path $Path -ContractorsOU $ContractorsOU -DisabledUsersOU $DisabledUsersOU
    Sleep -Seconds 5
    GetContractorsWithoutX -Path $Path -ContractorsOU $ContractorsOU
}