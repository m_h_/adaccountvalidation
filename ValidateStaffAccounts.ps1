﻿<#
.SYNOPSIS
  Component of REDACTED
  Script for validating mailboxes
 
.DESCRIPTION
  Disable any inactive staff accounts
  Output results to CSV files
 
.INPUTS
  <None>
 
.OUTPUTS
  $PSScriptRoot\Account Validation\dd-MMM-yyyy-HH-mm-ss\oldStaffAccounts.csv

.NOTES
  Version:        1.0
  Author:         Matt Hanson
  Creation Date:  29/03/2019
  Purpose/Change: Initial Scripting
 
.EXAMPLE
  
  ValidateStaffAccounts
#>

Function DisableInactiveStaffAccounts {
    ####################################################################
    # Disable any inactive staff accounts
    # Move to Disabled OU
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AccountsOU,
        [string] $DisabledUsersOU
    )

    $currentDateTime = Get-Date
    $inactiveStaffAccounts = Get-ADUser -Properties LastLogonDate -SearchBase $AccountsOU `
        -Filter * | Where-Object LastLogonDate -LT ($currentDateTime).AddMonths(-3) `
        | Where-Object LastLogonDate -ne $null
        
    
    # Disable accounts
    ForEach($inactiveStaff in $inactiveStaffAccounts) {
        Disable-ADAccount -Identity $inactiveStaff.SamAccountName -WhatIf
    }

    # Move to Disabled OU
    ForEach($user in $inactiveAdminAccounts) {
        $GUID = (Get-ADUser $user).ObjectGUID
        Move-ADObject -Identity $GUID -TargetPath $DisabledUsersOU -WhatIf
    }

    # Output results
    $inactiveStaffAccounts | Export-Csv -Path "$($Path)\inactiveStaffAccounts.csv"

    Write-Host "Staff that have not logged in for over 3 months have been disabled." `
        -ForegroundColor Yellow
    Write-Host "See '$($Path)\oldStaffAccounts.csv' for Staff that have been disabled." `
        -ForegroundColor Yellow
}

Function DisableUnusedStaffAccounts {
    ####################################################################
    # Get Staff that have no LastLogonDate value
    # Disable them
    # Move to Disabled OU
    # Output results
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AdminsOU,
        [string] $DisabledUsersOU
    )

    $unusedStaffAccounts = Get-ADUser -Properties LastLogonDate `
        -Filter { Enabled -eq $true } -SearchBase $AdminsOU `
        | Where-Object LastLogonDate -eq $null
        
    # Disable accounts
    ForEach($unusedStaff in $unusedStaffAccounts) {
        Disable-ADAccount -Identity $unusedStaff.SamAccountName -WhatIf
    }
    
    # Move to Disabled OU
     ForEach($user in $unusedStaffAccounts) {
        $GUID = (Get-ADUser $user).ObjectGUID
        Move-ADObject -Identity $GUID -TargetPath $DisabledUsersOU -WhatIf
    }

    # Output results to basic csv
    $unusedStaffAccounts | Export-Csv -Path "$($Path)\unusedStaffAccounts.csv"

    Write-Host "See '$($Path)\unusedStaffAccounts.csv' for Staff that have never logged on" `
        -ForegroundColor Yellow
}

Function ValidateStaffAccounts {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
        [string] $Path,
        [string] $AccountsOU,
        [string] $AdminsOU,
        [string] $DisabledUsersOU
    )
    
    DisableInactiveStaffAccounts -Path $Path -AccountsOU $AccountsOU -DisabledUsersOU $DisabledUsersOU
    DisableUnusedStaffAccounts -Path $Path -AdminsOU $AdminsOU -DisabledUsersOU $DisabledUsersOU
}
 
