﻿<#
.SYNOPSIS
  Main for this task REDACTED
  Script for validation Active Directory Accounts
 
.DESCRIPTION
  Call component functions 
 
.INPUTS
  <None>
 
.OUTPUTS
  New Directory at: $PSScriptRoot\Account Validation\$Time
   
.NOTES
  Version:        1.0
  Author:         Matt Hanson
  Creation Date:  29/03/2019
  Purpose/Change: Initial Scripting
 
.EXAMPLE
  ADAccountValidation
#>


[CmdletBinding()]
param(
    [parameter(
        Mandatory=$false,
        ValueFromPipelineByPropertyName=$false
    )]
    [PSCredential] $credential
)

# Functions
Function ADAccountValidation {    
    Add-Type -AssemblyName System.IO.Compression.FileSystem


    # Get current time, set Path for output, Create directory
    $Time =  Get-Date -format "yyyy-MM-dd-HH-mm" # Used in filename 
    $Folder = "$PSScriptRoot\Account Validation\"
    $FilePath = Join-Path $Folder $Time
    New-Item -Path $Folder -Name $Time -ItemType "directory"

    # Active Directory OUs
    $AccountsOU = "OU=REDACTED,DC=REDACTED,DC=COM,DC=AU"
    $AdminsOu = "OU=REDACTED,OU=REDACTED,DC=REDACTED,DC=COM,DC=AU"
    $ContractorsOU = "OU=REDACTED,OU=REDACTED,DC=REDACTED,DC=COM,DC=AU"
    $DisabledUsersOU = "OU=REDACTED,OU=REDACTED,DC=REDACTED,DC=COM,DC=AU"
    $MailboxesOU = "OU=REDACTED,OU=REDACTED,DC=REDACTED,DC=COM,DC=AU"
    $RootOU = "DC=REDACTED,DC=COM,DC=AU"

    # Call functions
    ValidateAdminAccounts -Path $FilePath -AdminsOU $AdminsOU -DisabledUsersOU $DisabledUsersOU -RootOU $RootOU
    ValidateContractorsOU -Path $FilePath -ContractorsOU $ContractorsOU -DisabledUsersOU $DisabledUsersOU
    ValidateMailboxes -Path $FilePath -MailboxesOU $MailboxesOU -RootOU $RootOU
    ValidateStaffAccounts -Path $FilePath -AccountsOU $AccountsOU -AdminsOU $AdminsOU

    # Create ticket in Jira at: REDACTED
    $Body = [PSCustomObject]@{
        fields=[PSCustomObject]@{
        project=[PSCustomObject]@{
            key="ITDC"
        };
        summary="AD Account Validation Report";
        description = "Testing output to jira from AD for REDACTED";
        issuetype=[pscustomobject]@{
            name="Task";
        };
        labels="audit","compliance";
        }
    } | ConvertTo-Json           
 
    $JiraIssue = new-JiraIssue -Body $Body

    Add-Type -AssemblyName 'System.Net.Http'
     
    $Files = Get-ChildItem $FilePath
    ForEach ($File in $Files) {
        set-JiraFileAttachment -Path $File.FullName -IssueKey $JiraIssue.Output.key | Out-Null
    }

}
